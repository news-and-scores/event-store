import AJV from 'ajv'
import * as FileSystem from 'fs'
import * as Path from 'path'


const ajv =
    new AJV();

const compiledPublishSchema =
    ajv.compile(JSON.parse(FileSystem.readFileSync(Path.join(__dirname, '/schemas/Publish.json'), {encoding: 'utf-8'})));


export const validatePublish = data => {
    const valid =
        compiledPublishSchema(data);

    if (valid)
        return undefined;
    else
        return compiledPublishSchema.errors;
};