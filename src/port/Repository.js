import {MongoClient, ObjectID} from "mongodb";


const client = () =>
    new Promise((resolve, reject) => {
        try {
            resolve(new MongoClient(process.env.MONGO_URI || "mongodb://localhost:27017/thepolyglotdeveloper", {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }));
        } catch (e) {
            reject(e);
        }
    });


const find = (collection, query, count) => {
    const callback = (resolve, reject) => (err, docs) => {
        if (err === null) {
            docs.forEach(doc => {
                doc.id = doc._id;
                delete doc._id;
            });

            resolve(docs);
        } else
            reject(err);
    };

    return new Promise((resolve, reject) =>
        count === undefined
            ? collection.find(query, {sort: {_id: 1}}).toArray(callback(resolve, reject))
            : collection.find(query, {sort: {_id: 1}}).limit(count).toArray(callback(resolve, reject))
    );
};


const findOne = (collection, query) =>
    new Promise((resolve, reject) => {
        try {
            resolve(collection.findOne(query));
        } catch (e) {
            reject(e);
        }
    });


// insertMany :: (Mongo:Collection, List<X>) -> Promise(Mongo:Error, Mongo:InsertManyResult)
const insertMany = (collection, docs) =>
    new Promise((resolve, reject) => {
        try {
            resolve(collection.insertMany(docs));
        } catch (e) {
            reject(e);
        }
    });


const dropCollection = (db, collectionName) =>
    new Promise((resolve, reject) => {
        try {
            db.dropCollection(collectionName, (err, okay) => {
                if (err && err.codeName !== 'NamespaceNotFound')
                    reject(err);
                else
                    resolve(okay);
            });
        } catch (e) {
            reject(e);
        }
    });


// connect :: () -> Promise<Mongo:Error, { ... operations }>
export const connect = () =>
    client()
        .then(client =>
            new Promise((resolve, reject) => {
                client.connect(err => {
                    if (err === null) {
                        resolve(client.db());
                    } else {
                        reject(err);
                    }
                });
            }).then(db => ({
                    events: args => {
                        const query = args.query || {};

                        if (args.from) {
                            query._id = {$gt: new ObjectID(args.from)}
                        }

                        return find(db.collection('events'), query, args.limit)
                    },
                    truncateEvents: () => dropCollection(db, 'events'),
                    publishEvents: events =>
                        events.length === 0
                            ? Promise.resolve([])
                            : insertMany(db.collection('events'), events)
                                .then(result => Object.values(result.insertedIds)),
                    close: () => client.close()
                })
            )
        );
