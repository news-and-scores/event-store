import * as hapi from "@hapi/hapi";
import * as Boom from "@hapi/boom";

import * as Services from "../Services";

import {validatePublish} from "./Schemas";


export const start = (repository, configuration) => {
    const init = async () => {
        const info =
            configuration.info || false;

        delete configuration.info;

        const server =
            new hapi.Server(configuration);

        await server.register({
            plugin: require('@hapi/inert')
        });


        server.route({
            method: 'GET',
            path: '/api/events',
            handler: request => {
                const query =
                    request.query;

                const options =
                    {};

                const limit =
                    oneAndOnlyOne(query, 'limit', undefined);

                if (limit !== undefined) {
                    const value =
                        Number(limit);

                    if (isNaN(value)) {
                        throw Boom.preconditionFailed(`limit is not a number: '${limit}'`);
                    } else {
                        options.limit = value;
                    }
                }

                const from =
                    oneAndOnlyOne(query, 'from', undefined);

                if (from !== undefined) {
                    options.from = from;
                }

                const q =
                    oneAndOnlyOne(query, 'query', undefined);

                if (q !== undefined) {
                    try {
                        options.query = JSON.parse(q);
                    } catch (e) {
                        throw Boom.preconditionFailed(`query is not valid JSON: '${e}'`);
                    }
                }

                return Services.events(repository, options);
            }
        });

        server.route({
            method: 'POST',
            path: '/api/publish',
            handler: request => {
                const payload =
                    request.payload;

                const errors =
                    validatePublish(payload);

                if (errors)
                    throw Boom.preconditionFailed(errors);
                else
                    return Services.publish(repository, payload);
            }
        });


        await server.start();

        if (info) {
            console.log('Server running at:', server.info.uri);
        }

        return server;
    };


    const server =
        init();

    return server.then(() => ({
        stop: options =>
            server
                .then(s => s.stop(options))
                .then(() => repository.close())
    }));
};


const oneAndOnlyOne = (params, name, defaultValue) => {
    const value =
        params[name];

    return Array.isArray(value) ? value[value.length - 1]
        : value === undefined ? defaultValue
            : value;
};