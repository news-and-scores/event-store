export const events = (repository, args) =>
    repository.events(Object.assign({}, {
        limit: 1000
    }, args));


export const publish = (repository, events) =>
    repository.publishEvents(events);