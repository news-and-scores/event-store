import {expect} from "chai"
import * as Repository from '../../src/port/Repository'


describe('validating mongo on gitlab', () => {
    it('should have the correct environment variables', () => {
        if (process.env.NODE_ENV === 'test') {
            expect(process.env.MONGO_URI).to.equal('mongodb://mongo/event_store');
        }
    });

    it('should connect to mongo', () => {
        let allOkay =
            false;

        return Repository
            .connect()
            .then(repository => {
                allOkay = true;
                return repository.close();
            })
            .finally(() => expect(allOkay).to.equal(true));
    });
});


describe('events', () => {
    it('should limit the number of events based on the passed argument', () => {
        let events =
            undefined;

        return Repository
            .connect()
            .then(repository =>
                repository.truncateEvents()
                    .then(() => populateEvents(repository))
                    .then(() => repository.events({limit: 10}))
                    .then(result => events = result)
                    .catch(console.error)
                    .finally(() => {
                        repository.close();

                        expect(events.length).to.equal(10);

                        expect(events[0].name).to.equal('EventA');
                        expect(events[0].data).to.deep.equal({value1: 0, value2: 'Hello 0'});
                        expect(events[1].name).to.equal('EventB');
                        expect(events[1].data).to.deep.equal({value1: 1, value2: 'Hello 1'});

                        expect(events[9].name).to.equal('EventB');
                        expect(events[9].data).to.deep.equal({value1: 9, value2: 'Hello 4'});
                    })
            );
    });

    it('should return events from a specific ID', () => {
        let events =
            undefined;

        return Repository
            .connect()
            .then(repository =>
                repository.truncateEvents()
                    .then(() => populateEvents(repository))
                    .then(() => repository.events({limit: 10}))
                    .then(result => repository.events({limit: 10, from: result[9].id}))
                    .then(result => events = result)
                    .catch(console.error)
                    .finally(() => {
                        repository.close();

                        expect(events.length).to.equal(10);

                        expect(events[0].name).to.equal('EventA');
                        expect(events[0].data).to.deep.equal({value1: 10, value2: 'Hello 0'});
                        expect(events[1].name).to.equal('EventB');
                        expect(events[1].data).to.deep.equal({value1: 11, value2: 'Hello 1'});

                        expect(events[9].name).to.equal('EventB');
                        expect(events[9].data).to.deep.equal({value1: 19, value2: 'Hello 4'});
                    })
            );
    });

    it('should return a stream of events limited based on the query', () => {
        let events =
            undefined;

        return Repository
            .connect()
            .then(repository =>
                repository.truncateEvents()
                    .then(() => populateEvents(repository))
                    .then(() => repository.events({limit: 10}))
                    .then(result => repository.events({limit: 10, from: result[9].id, query: {name: 'EventA', 'data.value2': 'Hello 4'}}))
                    .then(result => events = result)
                    .catch(console.error)
                    .finally(() => {
                        repository.close();

                        expect(events.length).to.equal(10);

                        expect(events[0].name).to.equal('EventA');
                        expect(events[0].data).to.deep.equal({value1: 14, value2: 'Hello 4'});
                        expect(events[1].name).to.equal('EventA');
                        expect(events[1].data).to.deep.equal({value1: 24, value2: 'Hello 4'});

                        expect(events[9].name).to.equal('EventA');
                        expect(events[9].data).to.deep.equal({value1: 104, value2: 'Hello 4'});
                    })
            );
    })
});


const populateEvents = repository => {
    const events =
        [];

    for (let lp = 0; lp < 1000; lp += 1) {
        events.push({
            when: new Date().getTime(),
            name: lp % 2 === 0 ? "EventA" : "EventB",
            version: '1',
            data: {
                value1: lp,
                value2: `Hello ${lp % 5}`
            }
        });
    }

    return repository.publishEvents(events);
};