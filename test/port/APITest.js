import * as Repository from '../../src/port/Repository'
import * as API from '../../src/port/API'

import fetch from 'node-fetch'

import {expect} from "chai"


const PORT =
    8000;


describe('/api/events', () => {
    it('when called without any from, limit or query should return 1000 events from the start of the stream', () => {
        let events =
            undefined;

        return setup()
            .then(server => get(`http://localhost:${PORT}/api/events`)
                .then(result => events = result)
                .then(() => server.stop())
                .catch(console.error)
                .finally(() => {
                    expect(events.length).to.equal(1000);
                }));
    });

    it('when called with only limit 100 should return 100 events from the start of the stream', () => {
        let events =
            undefined;

        return setup()
            .then(server =>
                get(`http://localhost:${PORT}/api/events?limit=100`)
                    .then(result => events = result)
                    .then(() => server.stop())
                    .catch(console.error)
                    .finally(() => {
                        expect(events.length).to.equal(100);
                    }));
    });

    it('when called with from should return the events following that id', () => {
        let events =
            undefined;

        return setup()
            .then(server =>
                get(`http://localhost:${PORT}/api/events?limit=100`)
                    .then(result => get(`http://localhost:${PORT}/api/events?limit=1&from=${result[99].id}`))
                    .then(result => events = result)
                    .then(() => server.stop())
                    .catch(console.error)
                    .finally(() => {
                        expect(events.length).to.equal(1);
                        expect(events[0].data.value1).to.equal(100);
                    }));
    });

    it('when called with query should return a stream of events that match the query constraints', () => {
        let events =
            undefined;

        return setup()
            .then(server =>
                get(`http://localhost:${PORT}/api/events?limit=100`)
                    .then(result => get(`http://localhost:${PORT}/api/events?limit=100&from=${result[99].id}&query={"name": "EventA", "data.value2": "Hello 4"}`))
                    .then(result => events = result)
                    .then(() => server.stop())
                    .catch(console.error)
                    .finally(() => {
                        expect(events.length).to.equal(100);

                        expect(events.filter(n => n.name !== 'EventA').length).to.equal(0);
                        expect(events.filter(n => n.data.value2 !== 'Hello 4').length).to.equal(0);
                    }));
    });

    it('when the result is the same size as the limit then the result should be cachable');
    it('when the result is the less than the size of the limit then the result should not be cachable');

    it('when limit is not a valid positive integer then an error is returned', () => {
        let result =
            undefined;

        return setup()
            .then(server =>
                fetch(`http://localhost:${PORT}/api/events?limit=aa00`)
                    .then(r => result = r)
                    .then(() => server.stop())
                    .catch(console.error)
                    .finally(() => {
                        expect(result.status).to.equal(412);
                    }));
    });

    it('when query can not be parsed into a JSON object then an error is returned', () => {
        let result =
            undefined;

        return setup()
            .then(server =>
                fetch(`http://localhost:${PORT}/api/events?limit=100&query={'name': 'EventA'}`)
                    .then(r => result = r)
                    .then(() => server.stop())
                    .catch(console.error)
                    .finally(() => {
                        expect(result.status).to.equal(412);
                    }));
    });
});


describe('/api/publish', () => {
    it('when publishing multiple events all event IDs are returned and all events are appended to the end of the stream', () => {
        let ids =
            undefined;

        let events =
            undefined;

        const zip = (arr1, arr2) =>
            arr1.map((k, i) => [k, arr2[i]]);

        return setup(20)
            .then(server =>
                fetch(`http://localhost:${PORT}/api/publish`, {
                    method: 'post',
                    body: JSON.stringify([
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1000,
                                value2: 'Hello 1'
                            }
                        },
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1001,
                                value2: 'Hello 2'
                            }
                        },
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1002,
                                value2: 'Hello 3'
                            }
                        },
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1003,
                                value2: 'Hello 4'
                            }
                        },
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1004,
                                value2: 'Hello 5'
                            }
                        }
                    ]),
                    headers: {'Content-Type': 'application/json'}
                })
                    .then(result => result.json())
                    .then(result => {
                        ids = result;
                        return get(`http://localhost:${PORT}/api/events`)
                    })
                    .then(result => {
                        events = result;
                        server.stop();
                    })
                    .catch(console.error)
                    .finally(() => {
                        expect(ids.length).to.equal(5);
                        expect(events.length).to.equal(25);

                        expect(zip(events.slice(20).map(x => x.id), ids).filter(x => x[0] === x[1]).length).to.equal(5);
                    }));
    });

    it('when attempting to publish events with extra fields an error is returned', () => {
        let result =
            undefined;

        return setup(20)
            .then(server =>
                fetch(`http://localhost:${PORT}/api/publish`, {
                    method: 'post',
                    body: JSON.stringify([
                        {
                            name: 'EventC',
                            version: '1',
                            something: 123,
                            data: {
                                value1: 1000,
                                value2: 'Hello 1'
                            }
                        },
                        {
                            name: 'EventC',
                            version: '1',
                            else: 123,
                            data: {
                                value1: 1001,
                                value2: 'Hello 2'
                            }
                        }
                    ]),
                    headers: {'Content-Type': 'application/json'}
                })
                    .then(r => {
                        result = r;
                        server.stop();
                    })
                    .catch(console.error)
                    .finally(() => {
                        expect(result.status).to.equal(412);
                    }));
    });

    it('when attempting to publish events with fields missing an error is returned', () => {
        let result =
            undefined;

        return setup(20)
            .then(server =>
                fetch(`http://localhost:${PORT}/api/publish`, {
                    method: 'post',
                    body: JSON.stringify([
                        {
                            name: 'EventC',
                            version: '1',
                            data: {
                                value1: 1000,
                                value2: 'Hello 1'
                            }
                        },
                        {
                            name: 'EventC',
                            data: {
                                value1: 1001,
                                value2: 'Hello 2'
                            }
                        }
                    ]),
                    headers: {'Content-Type': 'application/json'}
                })
                    .then(r => {
                        result = r;
                        server.stop();
                    })
                    .catch(console.error)
                    .finally(() => {
                        expect(result.status).to.equal(412);
                    }));
    });

    it('when publishing no events the stream is left unaffected', () => {
        let ids =
            undefined;

        let events =
            undefined;

        return setup(20)
            .then(server =>
                fetch(`http://localhost:${PORT}/api/publish`, {
                    method: 'post',
                    body: JSON.stringify([]),
                    headers: {'Content-Type': 'application/json'}
                })
                    .then(result => result.json())
                    .then(result => {
                        ids = result;
                        return get(`http://localhost:${PORT}/api/events`)
                    })
                    .then(result => {
                        events = result;
                        server.stop();
                    })
                    .catch(console.error)
                    .finally(() => {
                        expect(ids.length).to.equal(0);
                        expect(events.length).to.equal(20);
                    }));
    });
});


const setup = (numberOfEvents = 2000) =>
    Repository
        .connect()
        .then(repository =>
            repository.truncateEvents()
                .then(() => populateEvents(repository, numberOfEvents))
                .then(() => API.start(repository, {host: 'localhost', port: PORT})));


const populateEvents = (repository, numberOfEvents) => {
    const events =
        [];

    for (let lp = 0; lp < numberOfEvents; lp += 1) {
        events.push({
            when: new Date().getTime(),
            name: lp % 2 === 0 ? "EventA" : "EventB",
            version: '1',
            data: {
                value1: lp,
                value2: `Hello ${lp % 5}`
            }
        });
    }

    return repository.publishEvents(events);
};


const get = url =>
    fetch(url)
        .then(x => x.json());

