import {expect} from "chai"

import * as Services from "../src/Services"


describe('events', () => {
    it('no passed arguments so defaults are passed through to the repository', () => {
        let args =
            undefined;

        const repository = {
            events: (a => {
                args = a;
                return Promise.resolve();
            })
        };


        return Services.events(repository)
            .finally(() => {
                expect(args.from).is.equal(undefined);
                expect(args.limit).is.equal(1000);
                expect(args.query).is.equal(undefined);
            });
    });

    it('a limit and from is passed and this same limit is passed through to the repository', () => {
        let args =
            undefined;

        const repository = {
            events: (a => {
                args = a;
                return Promise.resolve();
            })
        };


        return Services.events(repository, {limit: 100, from: '123', query: {name: 'EventA', 'data.value2': 'Hello 4'}})
            .finally(() => {
                expect(args.from).is.equal('123');
                expect(args.limit).is.equal(100);
                expect(args.query).is.deep.equal({name: 'EventA', 'data.value2': 'Hello 4'});
            });
    });
});